let myPokemon={
	name:'Pikachu',
	level:3,
	health:100,
	attack:50,
	tackle:function(){
		console.log('This pokemon tackled targetPokemon')
	},

	faint:function(){
		console.log('Pikachu fainted')
	}

}

// Using Object Constructor
function Pokemon(name,level){
	// properties
	this.name=name;
	this.level=level;
	this.health=3*level;
	this.attack=level;
	// methods
	this.tackle=function(target){
		console.log(this.name +' tackled '+ target.name)
		target.health=target.health-this.attack;
		console.log(target.name+`'s health now reduced to `+ target.health)
		if(target.health<=5){
			target.faint()
		}
	};

	this.faint=function(){
		
			console.log(this.name+' fainted')
		
		
	}
}

// Create new instance of the "Pokemon" object each with their unique properties
let pikachu = new Pokemon('Pikachu',16)
let squirtle=new Pokemon('Squirtle', 8)
let charmander = new Pokemon('Charmander', 8)

pikachu.tackle(squirtle)

pikachu.tackle(squirtle)

pikachu.tackle(charmander)

console.log(charmander)
console.log(squirtle)